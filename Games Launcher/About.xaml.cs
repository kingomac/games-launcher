﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;

namespace Games_Launcher
{
    /// <summary>
    /// Lógica de interacción para About.xaml
    /// </summary>
    public partial class About : Window
    {
        public Style style = new Style();
        public About()
        {
            InitializeComponent();
        }

        private void Label1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Process.Start("https://bitbucket.org/kingomac/games-launcher/src");
        }

        private void Label1_MouseEnter(object sender, MouseEventArgs e)
        {

        }
    }
}
