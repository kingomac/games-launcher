﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;
using Microsoft.Win32;
using Microsoft.CSharp;

namespace Games_Launcher
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string pName;
        public MainWindow()
        {
            InitializeComponent();
        }

        public bool clearTasksBool = false;
        public void CheckBox1_Checked(object sender, RoutedEventArgs e)
        {
            clearTasksBool = true;
        }
        public void CheckBox1_UnChecked(object senderm, RoutedEventArgs e)
        {
            clearTasksBool = false;
        }

        public bool additionalProgram1 = false;
        public void Checkbox_Checked(object sender, RoutedEventArgs e)
        {
            additionalProgram1 = true;
        }
        public void Checkbox_UnChecked(object sender, RoutedEventArgs e)
        {
            additionalProgram1 = false;
        }

        public void Button_Click(object sender, RoutedEventArgs e)
        {
            string addProg1Dir = textBox.Text;  //Directorio programa1
            string gameDirectoy = textBox1.Text;  //Directorio del juego
            bool launchGame = true;

            if (additionalProgram1)
            {
                if (File.Exists(addProg1Dir))
                {
                    Process gameProc = Process.Start(addProg1Dir);
                    gameProc.WaitForExit();
                    RestartTasks("restartTasks.cmd");
                    launchGame = true;
                }
                else
                {
                    MessageBox.Show("Program doesn't exists", "File is missing!", MessageBoxButton.OK, MessageBoxImage.Error);
                    launchGame = false;
                }
            }
            if (clearTasksBool)
            {
                if (!File.Exists("clearTasksCmd.cmd") || !File.Exists("restartTasks.cmd") || !File.Exists("clearTasksCmd.cmd") && !File.Exists("restartTasks.cmd"))
                {
                    MessageBox.Show("Did you remove some files? Check it", "Program files missing", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    ClearTasks("clearTasksCmd.cmd"); //Directorio de los cmd's
                }

            }
            if (launchGame)
            {
                if (File.Exists(gameDirectoy))
                {
                    Process.Start(gameDirectoy);    //Ejecutar el juego
                }
                else
                {
                    MessageBox.Show("Game doesn't exists", "Game is missing!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        static void ClearTasks(string clearTasksCmd)
        {
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + clearTasksCmd)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                // *** Redirect the output ***
                RedirectStandardError = true,
                RedirectStandardOutput = true
            };

            process = Process.Start(processInfo);
            process.WaitForExit();
            process.Close();
        }
        static void RestartTasks(string restartTasksCmd)
        {
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + restartTasksCmd)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                // *** Redirect the output ***
                RedirectStandardError = true,
                RedirectStandardOutput = true
            };

            process = Process.Start(processInfo);
            process.WaitForExit();
            process.Close();
        }

        public void textBox2_TextChanged(object sender, TextChangedEventArgs e)
        {
            pName = textBox2.Text;
        }
        public void DetectProcesses()
        {
            Process[] gameProcess = Process.GetProcessesByName(pName);
            if (gameProcess.Length == 0)
            {
                textBlock.Text = "Waiting game";
            }
            else
            {
                textBlock.Text = "Game running";
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            DetectProcesses();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                Filter = "Executable (*.exe)|*.exe|All files (*.*)|*.*",
                FilterIndex = 1,

                Multiselect = false
            };
            // Call the ShowDialog method to show the dialog box.
            bool? userClickedOK = openFileDialog1.ShowDialog();
            if (userClickedOK == true)
            {
                // Open the selected file to read.
                string fileStream = openFileDialog1.FileName;

                textBox1.Text = fileStream;
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                Filter = "Executable (*.exe)|*.exe|All files (*.*)|*.*",
                FilterIndex = 1,

                Multiselect = false
            };
            // Call the ShowDialog method to show the dialog box.
            bool? userClickedOK = openFileDialog1.ShowDialog();
            if (userClickedOK == true)
            {
                // Open the selected file to read.
                string fileStream = openFileDialog1.FileName;

                textBox.Text = fileStream;
            }
        }
        public string profile;

        public void button4_Click(object sender, RoutedEventArgs e)
        {
            SetProfile();
            SaveProfile();
        }
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            SetProfile();
            LoadProfile();
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        public void SaveProfile()
        {
            TextWriter save = new StreamWriter(profile + ".txt");
            save.WriteLine(textBox1.Text); //Game path
            save.WriteLine(textBox2.Text); //Game name
            save.WriteLine(textBox.Text); //Prog path
            save.WriteLine(checkBox.IsChecked.ToString()); //Prog active
            save.WriteLine(checkBox1.IsChecked.ToString()); //Unnecessary tasks
            save.Close();
        }
        public void SetProfile()
        {
            if (Convert.ToBoolean(radioButton.IsChecked)) profile = "profile1";
            else if (Convert.ToBoolean(radioButton1.IsChecked)) profile = "profile2";
            else if (Convert.ToBoolean(radioButton2.IsChecked)) profile = "profile3";
            else if (Convert.ToBoolean(radioButton3.IsChecked)) profile = "profile4";
            else if (Convert.ToBoolean(radioButton4.IsChecked)) profile = "profile5";
            else if (Convert.ToBoolean(radioButton5.IsChecked)) profile = "profile6";
            else if (Convert.ToBoolean(radioButton6.IsChecked)) profile = "profile7";
            else if (Convert.ToBoolean(radioButton7.IsChecked)) profile = "profile8";
            else if (Convert.ToBoolean(radioButton8.IsChecked)) profile = "profile9";
            else if (Convert.ToBoolean(radioButton9.IsChecked)) profile = "profile10";
            else profile = "";
        }
        public void LoadProfile()
        {
            if(File.Exists(profile + ".txt"))
            {
                TextReader load = new StreamReader(profile + ".txt");
                textBox1.Text = load.ReadLine();
                textBox2.Text = load.ReadLine();
                textBox.Text = load.ReadLine();
                checkBox.IsChecked = Convert.ToBoolean(load.ReadLine());
                checkBox1.IsChecked = Convert.ToBoolean(load.ReadLine());
                load.Close();
            }
            else
            {
                MessageBox.Show("Games launcher coudn't find the profile file.", "Profile doesn't exists", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void button6_Click(object sender, RoutedEventArgs e)
        {

        }

        private void button6_Click_1(object sender, RoutedEventArgs e)
        {
            RestartTasks("restartTasks.cmd");
        }

        private void button7_Click(object sender, RoutedEventArgs e)
        {
            About  win2 = new About();
            win2.Show();
        }
    }
}